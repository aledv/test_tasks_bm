const addModificationBtn = document.getElementById('addNewModification');
const attributes = document.getElementById('attributes');

addNewModificationFields();
addModificationBtn.addEventListener('click', addNewModificationFields);

function addNewModificationFields() {
    const attributes = document.getElementById('attributes');
    let countAttributes = attributes.querySelectorAll('input[name^=attribute_name_]').length;

    const attrNameBlock = '<div class="col-xs-12 col-md-6">\n' +
        '                    <label>Название модификации</label>\n' +
        '                    <input class="form-control" type="text" name="attribute_name_new' + (countAttributes + 1) + '" />\n' +
        '                </div>';

    const attrValueBlock = '<div class="col-xs-12 col-md-6">\n' +
        '                    <label>Значение модификации</label>\n' +
        '                    <input class="form-control" type="text" name="attribute_value_new' + (countAttributes + 1) + '" />\n' +
        '                </div>';

    attributes.insertAdjacentHTML('beforeend', attrNameBlock);
    attributes.insertAdjacentHTML('beforeend', attrValueBlock);
}