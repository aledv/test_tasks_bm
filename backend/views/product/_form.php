<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')
        ->textInput(['maxlength' => true])
        ->label('Название товара')
    ?>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'price')
                ->textInput(['maxlength' => true, 'type' => 'number'])
                ->label('Цена товара')
            ?>
        </div>

        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'old_price')
                ->textInput(['maxlength' => true, 'type' => 'number'])
                ->label('Старая цена товара')
            ?>
        </div>
    </div>

    <?= $form->field($model, 'photo')
        ->fileInput()
        ->label('Фото товара')
    ?>

    <?php
        if (!empty($model->photo)) {
            $img = Yii::getAlias('@webroot' . $model::$pathToImages .  $model->photo);
            if (is_file($img)) {
                $url = Yii::getAlias('@web') . $model::$pathToImages . $model->photo;
                echo 'Уже загружено ', Html::a('изображение', $url, ['target' => '_blank']) . '. <br />',
                    'Удалить фото? ' .  Html::checkbox('photoDelete', false) ;
            }
        }
    ?>

    <?= $form->field($model, 'description')
        ->textarea(['rows' => 6])
        ->label('Описание товара')
    ?>

    <?php
        $attributeValues = $model->getProductAttributesValue()->all();
        $countAttributes = count($attributeValues);
    ?>

    <div class="form-group">
        <div class="row">
            <div id="attributes">
                <?php
                    if ($countAttributes > 0) {
                        foreach ($attributeValues as $attrValue) {
                            $value = $attrValue['value'];
                            $attribute = $attrValue->getProductAttribute()->one();
                            $name = $attribute['name'];
                ?>
                            <div class="mb-2">
                                <div class="col-xs-12 col-md-5">
                                    <label>Название модификации</label>
                                    <input class="form-control" type="text" name="attribute_name_<?= $name ?>" value="<?= $name ?>"/>
                                </div>
                                <div class="col-xs-12 col-md-5">
                                    <label>Значение модификации</label>
                                    <input class="form-control" type="text" name="attribute_value_<?= $name ?>" value="<?= $value ?>"/>
                                </div>
                                <div class="col-xs-12 col-md-2 text-center">
                                    <label>Удалить модификации</label>
                                    <input type="checkbox" name="attribute_name_delete_<?= $name ?>" />
                                </div>
                            </div>
                <?php
                        }
                    }
                ?>
            </div>

            <div class="col-xs-12">
                <span id="addNewModification" style="cursor: pointer;">Добавить модификатор</span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

