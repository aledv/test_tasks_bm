<?php


namespace backend\services\models;


use common\models\Product;
use common\models\ProductAttribute;
use common\models\ProductAttributeValue;
use Yii;

class ProductAttributeService
{
    /**
     * @param string $name
     * @param string|null $needDelete
     * @return ProductAttribute|null
     */
    private function saveAttribute(string $name): ?ProductAttribute
    {
        if (! $name) { return null;}

        $attribute = ProductAttribute::findOne(['name' => $name]) ?? new ProductAttribute();

        $attributeData = [
            'ProductAttribute' => ['name' => $name]
        ];

        if ($attribute->load($attributeData) && $attribute->validate()) {
            $attribute->save();
        }

        return $attribute ?? null;
    }


    /**
     * @param string $value
     * @param string|null $needDelete
     * @param Product $product
     * @param ProductAttribute $attribute
     * @return ProductAttributeValue|null
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function saveAttributeValue(string $value, ?string $needDelete, Product $product, ProductAttribute $attribute): ?ProductAttributeValue
    {
        if (! $value) { return null;}

        $attributeValue = ProductAttributeValue::findOne(['product_id' => $product->id]) ?? new ProductAttributeValue();

        if ($this->deleteAttribute($attributeValue, $needDelete)) {
            return null;
        }

        $attributeValueData = [
            'ProductAttributeValue' => [
                'product_id' => $product->id,
                'attribute_id' => $attribute->id,
                'value' => $value,
            ]
        ];

        if ($attributeValue->load($attributeValueData) && $attributeValue->validate()) {
            $attributeValue->save();
        }

        return $attributeValue ?? null;
    }

    /**
     * @param ProductAttributeValue $attributeValue
     * @param string|null $needDelete
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function deleteAttribute(ProductAttributeValue $attributeValue, ?string $needDelete): bool
    {
        if ($needDelete === 'on') {
            $attributeValue->delete();

            return true;
        }

        return false;
    }

    /**
     * @param Product $model
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function handlingAttributes(Product $model): void
    {
        $input = Yii::$app->request->post();

        foreach ($input as $key => $value) {
            if (strpos($key, 'attribute_name_') !== false) {

                $attrName = explode('attribute_name_', $key)[1];

                $name = $input['attribute_name_' . $attrName];
                $value = $input['attribute_value_' . $attrName];
                $needDelete = $input['attribute_name_delete_' . $attrName] ?? null;

                if (! $name || ! $value) { continue; }

                $attribute = $this->saveAttribute($name);

                if ($attribute !== null) {
                    $this->saveAttributeValue($value, $needDelete, $model, $attribute);
                }
            }
        }
    }

    /**
     * @param Product $model
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function clearAllAttributeValue(Product $model): void
    {
        foreach ($model->getProductAttributesValue()->all() as $attrValue) {
            $attrValue->delete();
        }
    }
}