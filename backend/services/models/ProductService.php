<?php

namespace backend\services\models;

use backend\services\ImageService;
use common\models\Product;
use Yii;

class ProductService
{
    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * @var ProductAttributeService
     */
    private $productAttributeService;

    public function __construct()
    {
        $this->imageService = new ImageService();
        $this->productAttributeService = new ProductAttributeService();
    }

    /**
     * @param Product $model
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function createModel(Product $model): bool
    {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->photo = $this->imageService->getUploadImagePath($model, 'photo');

            if ($model->save()) {
                $this->productAttributeService->handlingAttributes($model);
                return true;
            }
        }

        return false;
    }

    /**
     * @param Product $model
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateModel(Product $model): bool
    {
        $this->productAttributeService->handlingAttributes($model);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (Yii::$app->request->post()['photoDelete']) {
                $filePath = Yii::getAlias('@webroot' . $model::$pathToImages . $model->photo);
                $this->imageService->removeImage($filePath);
                $model->photo = '';
            } else {
                $model->photo = $this->imageService->replaceImage($model, 'photo');
            }

            if ($model->save()) {
                return true;
            }
        }

        return false;
    }

}