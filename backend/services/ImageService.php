<?php

namespace backend\services;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class ImageService
{
    /**
     * @param ActiveRecord $model
     * @param string $attribute
     * @return string
     */
    public function getUploadImagePath(ActiveRecord $model, string $attribute) {
        $photo = UploadedFile::getInstance($model, $attribute);

        if ($photo) {
            $name = md5(uniqid(rand(), true)) . '.' . $photo->extension;

            $filePath = Yii::getAlias('@webroot' . $model::$pathToImages . $name);
            if ($photo->saveAs($filePath)) {
                return $name;
            }
        }

        return '';
    }

    /**
     * @param ActiveRecord $model
     * @param string $attribute
     * @return mixed|string
     */
    public function replaceImage(ActiveRecord $model, string $attribute)
    {
        $oldPhoto = $model->photo;
        $newPhoto = $this->getUploadImagePath($model, $attribute);

        if ($newPhoto) {
            if (! empty($oldPhoto)) {
                $this->removeImage(Yii::getAlias('@webroot' . $model::$pathToImages . $oldPhoto));
            }

            return $newPhoto;
        }

        return $oldPhoto;
    }

    /**
     * @param string $filePath
     */
    public function removeImage(string $filePath) {
        if (! empty($filePath)) {
            if (is_file($filePath)) {
                unlink($filePath);
            }
        }
    }

}