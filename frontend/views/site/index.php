<?php

/* @var $this yii\web\View */

use frontend\assets\BackendAsset;
use yii\helpers\BaseStringHelper;


$backend = BackendAsset::register($this);
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <?php
                foreach ($products as $product) {
            ?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img class="product__image" src="<?= empty($product->photo) ? 'https://via.placeholder.com/350x150' : $backend->baseUrl . $product::$pathToImages . $product->photo ?> " alt="<?= $product->name ?>">
                        <div class="caption">
                            <h3><?= $product->name ?></h3>

                            <ul class="list-unstyled">
                                <li>Цена: <?= $product->price ?></li>
                                <?php if ($product->old_price) { ?>
                                    <li>Старая цена: <?= $product->price ?></li>
                                <?php } ?>
                                <?php if (strlen($product->description) > 0) { ?>
                                    <li>Описание:<?= BaseStringHelper::truncate($product->description, 100); ?></li>
                                <?php } ?>
                            </ul>

                            <a href="product/<?= $product->slug ?>" class="btn btn-success">Подробнее</a>
                        </div>
                    </div>
                </div>
            <?php
                }
            ?>
        </div>

    </div>
</div>
