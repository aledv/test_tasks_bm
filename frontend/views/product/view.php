<?php

/* @var $this yii\web\View */

use frontend\assets\BackendAsset;

$backend = BackendAsset::register($this);
$this->title = $product->name;

$productImage = empty($product->photo) ? 'https://via.placeholder.com/350x150' : $backend->baseUrl . $product::$pathToImages . $product->photo;

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
           <div class="col-sm-6 col-md-4">
               <a href="<?= $productImage ?>" target="_blank">
                    <img class="product__image" src="<?= $productImage ?> " alt="<?= $product->name ?>">
               </a>
            </div>
            <div class="col-sm-6 col-md-8">
                <div class="caption">
                    <h3><?= $product->name ?></h3>

                    <ul class="list-unstyled">
                        <li>Цена: <?= $product->price ?></li>
                        <?php if ($product->old_price) { ?> <li>Старая цена: <?= $product->price ?></li> <?php } ?>
                        <?php if (strlen($product->description) > 0) { ?>
                            <li>Описание:<?= $product->description ?></li>
                        <?php } ?>
                    </ul>

                    <?php if (count($attributeValues) > 0) { ?>

                        <h4>Модификации товара</h4>
                        <ul>
                            <?php
                                foreach ($attributeValues as $attrValue) {
                                    $attribute = $attrValue->getProductAttribute()->one();
                            ?>
                                <li>
                                    <a href="<?= $product->slug ?>/<?= $attribute->id ?>">
                                        <?= $attribute->name ?>: <?= $attrValue->value ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>

                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
</div>
