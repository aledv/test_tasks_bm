<?php

namespace frontend\services\models;

use common\models\Product;
use common\models\ProductAttribute;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class ProductService
{
    /**
     * @param string $slug
     * @return array
     * @throws HttpException
     */
    public function getProductViewParams(string $slug)
    {
        $product = Product::findOne(['slug' => $slug]);

        $this->isExist($product);

        $attributeValues = $product->getProductAttributesValue()->all();

        return [
            'product' => $product,
            'attributeValues' => $attributeValues
        ];
    }

    /**
     * @param string $slug
     * @param int $modificationId
     * @return array
     * @throws HttpException
     */
    public function getProductViewModificationParams(string $slug, int $modificationId)
    {
        $product = Product::findOne(['slug' => $slug]);

        $this->isExist($product);

        $attribute = ProductAttribute::findOne($modificationId);
        $attributeValue = $attribute->getProductAttributeValues()->one();

        return [
            'product' => $product,
            'attribute' => $attribute,
            'attributeValue' => $attributeValue,
        ];
    }

    /**
     * @param Product|null $product
     * @throws HttpException
     */
    private function isExist(?Product $product)
    {
        if (! $product) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}