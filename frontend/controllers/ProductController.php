<?php

namespace frontend\controllers;

use frontend\services\models\ProductService;
use yii\web\Controller;
use yii\web\HttpException;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->productService = new ProductService();
    }

    /**
     * @param string $slug
     * @return string
     * @throws HttpException
     */
    public function actionView(string $slug)
    {
        $params = $this->productService->getProductViewParams($slug);
        return $this->render('view', $params);
    }

    /**
     * @param string $slug
     * @param int $modificationId
     * @return string
     * @throws HttpException
     */
    public function actionViewModification(string $slug, int $modificationId)
    {
        $params = $this->productService->getProductViewModificationParams($slug, $modificationId);
        return $this->render('viewModification', $params);
    }
}
