<?php

use yii\db\Migration;

/**
 * Class m200815_193432_product
 */
class m200815_193432_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'price' => $this->decimal()->notNull(),
            'old_price' => $this->decimal()->null(),
            'photo' => $this->string()->null(),
            'description' => $this->text()->null(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
