<?php

namespace common\models;

use backend\services\ImageService;
use backend\services\models\ProductAttributeService;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property float $price
 * @property float|null $old_price
 * @property string|null $description
 * @property int $created_at
 * @property int $updated_at
 */
class Product extends ActiveRecord
{
    public static $pathToImages = '/images/products/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Название товара обязательно для заполнения'],
            [['price'], 'required', 'message' => 'Цена товара обязательна для заполнения'],

            [['price'], 'number', 'message' => 'Цена товара должна быть числом'],
            [['old_price'], 'number', 'message' => 'Старая цена товара должна быть числом'],

            [['description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributesValue()
    {
        return $this->hasMany(ProductAttributeValue::class, ['product_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
//    public function beforeDelete()
//    {
//        if (parent::beforeDelete()) {
//            $productAttributeService = new ProductAttributeService();
//            $productAttributeService->clearAllAttributes($this);
//
//            return true;
//        }
//
//        return false;
//    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete() {
        parent::afterDelete();

        $imageService = new ImageService();
        $filePath = Yii::getAlias('@webroot' . self::$pathToImages . $this->photo);
        $imageService->removeImage($filePath);

        $productAttributeService = new ProductAttributeService();
        $productAttributeService->clearAllAttributeValue($this);
    }
}
